#include <ros/ros.h>

#include <ros/console.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <geometry_msgs/AccelStamped.h>

void poseCallback(const geometry_msgs::PoseStamped& msg){
  ROS_INFO_STREAM("Pose" << msg);
}
// void TwistCallback(const geometry_msgs::TwistStamped::ConsPtr& msg){
//   printf ("Cloud: width = %d, height = %d\n", cloud->width, cloud->height);
// }
// void AccelCallback(const geometry_msgs::AccelStamped::ConsPtr& msg){
//   printf ("Cloud: width = %d, height = %d\n", cloud->width, cloud->height);
// }

int main(int argc, char** argv){
  ros::init(argc, argv, "pose_reader");
  ros::NodeHandle nh;
  std::string target;
  if (ros::param::get("~target", target)){
    ROS_INFO_STREAM("Got param: " << target);
  }
  else{
    ROS_ERROR("Failed to get param 'target'");
  }
  ros::Subscriber poseSub = nh.subscribe(target+"/pose", 1, poseCallback);
  // ros::Subscriber twistSub = nh.subscribe<geometry_msgs::TwistStamped>("velodyne_points", 1, callback);
  // ros::Subscriber accelSub = nh.subscribe<geometry_msgs::AccelStamped>("velodyne_points", 1, callback);
  ros::spin();
}
